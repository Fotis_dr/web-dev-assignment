/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs359db.servlets;

import cs359db.db.PhotosDB;
import cs359db.model.Photo;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Andreas
 */
public class GetImage extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                response.setContentType("text/html;charset=UTF-8");

        String photo_id = request.getParameter("photo_id");
        String metadata = request.getParameter("metadata");
        int number = Integer.parseInt(photo_id);
       System.out.println("eeee"+number);
       if(metadata.equals("false")){
                try ( OutputStream os = response.getOutputStream()) {
                    /* TODO output your page here. You may use following sample code. */

               byte[] imgData = PhotosDB.getPhotoBlobWithID(number);
                            response.setContentType("image/jpg");   // Use the appropriate type from 
                            // output with the help of outputStream
                      os.write(imgData);
                      os.flush();
                      os.close();       

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(GetImage.class.getName()).log(Level.SEVERE, null, ex);
                }
       }else if (metadata.equals("true")){
            String json_format="";
                try  {
                         /* TODO output your page here. You may use following sample code. */

                   Photo photo_info = PhotosDB.getPhotoMetadataWithID(number);
               //    System.out.println(photo_info.getUserName() +" "+photo_info.getContentType()+" "+photo_info.getTitle()+" "+photo_info.getNumberOfRatings()+photo_info.getDate());
                      response.setContentType("application/json");
                      
                     
                      if(photo_info==null){
                         json_format+="[]";
                      }else {
                         json_format+="["+photo_info.getUserName() +","+photo_info.getContentType()+","+photo_info.getTitle()+","+photo_info.getDate()+"]";
                      }               

                     } catch (ClassNotFoundException ex) {
                         Logger.getLogger(GetImage.class.getName()).log(Level.SEVERE, null, ex);
                     }
              //      {userName:”name”, title:”title”,
              //date:”timestamp”, contentType:”image/jpg”, numberOfRatings:”0”}
                     
                    try (PrintWriter out = response.getWriter()) {
                
                out.println(json_format);
                     }
       }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
